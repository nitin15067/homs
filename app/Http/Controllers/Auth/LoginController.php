<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{

    public function __construct()
    {
    }

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');
        
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                // $user->session_id = Session::getId();
                // $user->save();
                return response()->json([
                        'userInfo'=> [
                            'jwttoken' => "token",
                            'expires_at'=> "989898989898",
                            'authorities'=> ["GUEST"],
                            'details' => $user
                        ]
                        ], 200);
            }
            return response()->json([
                'error' => 'Login Failed',
                'details' => null
            ], 401);
    }
}
