import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

// Styles
import "./fontawesomeInit";
import "./assets/styles/scss/HoMS.scss";

// Ag grid
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

import Routes from "./routes";

// Store
import store from "./redux/store";

ReactDOM.render(
    <Provider store={store}>
        <Routes />
    </Provider>,
    document.getElementById("root")
);
