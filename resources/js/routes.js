import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {connect} from 'react-redux';
import { bindActionCreators } from "redux";

// Components
import Login from "./pages/login";
import Dashboard from "./pages/dashboard";
import Layout1 from "./layout/layout1/index";

// Actions
import {logout} from './redux/auth/action';

const Routes = props => {
    console.log(props.isLoggedIn);

    return (
        <Router>
            <Switch>
                <Route path="/">
                    {props.isLoggedIn ? <Layout1 logout={props.logout}><Dashboard /></Layout1> : <Login />}
                </Route>
            </Switch>
        </Router>
    );
};

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.authReducer.userInfo.isLoggedIn
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        logout
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
