import { Column, GridApi } from 'ag-grid-community';
import React, {useEffect, useState} from 'react';

// Helpers
import AgGridHelper from '../../helpers/agGridHelper';
import { gridOptionsSkippedQueue} from '../../helpers/gridOptionsHelper';
import {ColumnDefs} from '../../helpers/columnDefs/currentQueue';

// Mock Data
import {rowData1} from '../../helpers/mockData/currentSkippedQueue.mockData'

// Cell Renderer
import TokenFieldCurrentSkippedQueue from '../cellRenderers/tokenFieldCurrentSkippedQueue';

const currentSkippedQueue = () => {
    const [gridApiCurrentQueue, setGridApiCurrentQueue] = useState(null);
    const [currentQueueSelectedRow, setCurrentQueueSelectedRow] = useState(null);
    const [skippedQueueSelectedRow, setSippedQueueSelectedRow] = useState(null);


    let gridOptionsCurrentQueue = {
        rowData: rowData1,
        rowSelection: 'single',
        onSelectionChanged: (params) => {
            let selectedNodes = params.api.getSelectedNodes();
            if(selectedNodes.length === 1) 
                setCurrentQueueSelectedRow(selectedNodes[0].rowIndex);
        },
        frameworkComponents: {
            tokenFieldCurrentSkippedQueue: TokenFieldCurrentSkippedQueue
        },
        columnDefs: ColumnDefs(currentQueueSelectedRow),
        onGridReady: (params) => {
            setGridApiCurrentQueue(params.api);
            params.api.sizeColumnsToFit();
        }
    }

    useEffect(() => {
        console.log("outside condition");
        if(gridApiCurrentQueue){
            gridApiCurrentQueue.setColumnDefs(ColumnDefs(currentQueueSelectedRow));
            gridApiCurrentQueue.refreshCells({
                force: true
            });
        }
    }, [currentQueueSelectedRow]);

    return <div className="row">
        <div className="col-md-6">
            <AgGridHelper gridOptionsFromParent={gridOptionsCurrentQueue} />
        </div>
        <div className="col-md-6">

        <AgGridHelper gridOptionsFromParent={gridOptionsSkippedQueue} />
        </div>

    </div>
}

export default currentSkippedQueue;