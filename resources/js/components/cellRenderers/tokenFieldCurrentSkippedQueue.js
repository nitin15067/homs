import React, { useEffect } from 'react'

const TokenFieldCurrentSkippedQueue = (props) => {
    console.log("selected row index", props);
    const ActionBtns = () => {
        return <span className="float-right"><button type="button" className="btn btn-outline-primary btn-sm mr-2">Call</button><button type="button" className="btn btn-outline-info btn-sm">Skip</button></span>
    }

    return <span>{props.value} {props.rowIndex === props.selectedRowIndex ? <ActionBtns /> : null}</span>
}

export default TokenFieldCurrentSkippedQueue;