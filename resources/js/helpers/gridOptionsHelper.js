// Mock Data
import {rowData1} from './mockData/currentSkippedQueue.mockData';

// export const gridOptionsCurrentQueue = {
//     rowData: rowData1,
//     rowSelection: 'single',
//     onSelectionChanged: (params) => {
//         let selectedRows = params.api.getSelectedRows();
//         if(selectedRows.length === 1) {

//         }
//     },
//     columnDefs: [
//         { headerName: "Skipped Queue", field: "token", cellRenderer: }
//     ],
//     onGridReady: (params) => {
//         params.api.sizeColumnsToFit();
//     }

// }

export const gridOptionsSkippedQueue = {
    rowData: rowData1,
    columnDefs: [
        { headerName: "Skipped Queue", field: "token"}
    ],
    onGridReady: (params) => {
        params.api.sizeColumnsToFit();
    }
}