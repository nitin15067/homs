
export const ColumnDefs = (selectedRowIndex) => [
        { headerName: "Current Queue", 
        field: "token", 
        
        cellRendererParams: {
            selectedRowIndex
          },
          cellRenderer: 'tokenFieldCurrentSkippedQueue',
        filter: "agTextColumnFilter"
      }
    ]
  