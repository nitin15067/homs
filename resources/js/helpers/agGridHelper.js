import React from 'react';


// Ag grid
import {AgGridReact} from 'ag-grid-react';


const agGridHelper = ({gridOptionsFromParent}) => {
    console.log(gridOptionsFromParent)
    const gridOptions = {
        ...gridOptionsFromParent
    }

    return <div className="ag-theme-material vh-80 w-100">
    <AgGridReact
            gridOptions={gridOptions}
            />
    </div>
}

export default agGridHelper;