import axios from 'axios';

export const getUserInfoLS = (userInfoInit) => {
    const userInfoLS = localStorage.getItem("userInfo");
    try {
        const userInfoObj = JSON.parse(userInfoLS);
        const {expires_in, access_token} = userInfoObj.tokenDetails;
        if(new Date(expires_in) >  new Date()) {
            axios.defaults.headers.common["Authorization"] = `Bearer ${jwttoken}`
            return userInfoObj;
        }
        return userInfoInit;
    } catch(error) {
        return userInfoInit;
    }
}