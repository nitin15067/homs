import React, { useEffect, useState } from "react";

//Redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Components
import CurrentSkippedQueue from "../components/dashboard/currentSkippedQueue";
import RegistrationForm from "../components/dashboard/registrationForm";

const Dashboard = props => {
    const [currentQueue, setCurrentQueue] = useState([]);
    const [skippedQueue, setSkippedQueue] = useState([]);

    useEffect(() => {
        setInterval(() => {
            console.log("fetching queue from server");
        }, 5000);
    });


    return (
        <>
            <div className="container-fluid">
                {/* <CurrentSkippedQueue></CurrentSkippedQueue> */}
                <RegistrationForm></RegistrationForm>
            </div>
        </>
    );
};

function mapStateToProps(state) {
    return {
        ...state.authReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
