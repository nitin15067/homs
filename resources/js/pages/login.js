import React from "react";
import { Redirect } from "react-router-dom";

//Redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Components
import LoginComponent from "../components/auth/loginComponent";

// Actions
import { login } from "../redux/auth/action";

const Login = props => {
    if (props.redirectTo) return <Redirect to={props.redirectTo}></Redirect>;

    return (
        <div className="bg-gradient-primary vh-100 d-flex align-items-center">
            <LoginComponent login={props.login} />
        </div>
    );
};

function mapStateToProps(state) {
    return {
        ...state.authReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ login: login }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
