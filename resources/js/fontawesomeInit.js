import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faLaughWink,
    faStethoscope,
    faHospitalSymbol,
    faTachometerAlt,
    faForward,
    faPauseCircle,
    faSearch,
    faBars
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faLaughWink,
    faStethoscope,
    faBars,
    faForward,
    faSearch,
    faPauseCircle,
    faHospitalSymbol,
    faTachometerAlt
);
