import axios from "axios";
import {
    AUTH_LOGIN_FAILURE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_LOADING,
    AUTH_LOGOUT_FAILURE,
    AUTH_LOGOUT_SUCCESS,
    AUTH_LOGOUT_LOADING,
} from "../types";

export const login = data => {
    return dispatch => {
        dispatch({
            type: AUTH_LOGIN_LOADING
        });
        axios
            .post("/api/auth/login", data, {
                headers: {
                    "Content-Type": "application/json",
                    accept: "*/*"
                }
            })
            .then(response => {
                dispatch({
                    type: AUTH_LOGIN_SUCCESS,
                    payload: {
                        tokenDetails: response.data
                    }
                });
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: AUTH_LOGIN_FAILURE,
                    payload: {
                        error: `${error.message} ... ${error.data.error}`
                    }
                });
            });
    };
};

export const logout = () => {
    return dispatch => {
        dispatch({
            type: AUTH_LOGOUT_LOADING
        });
        let data = {
            
        }
        axios
            .post("/api/logout", data, {
                headers: {
                    "Content-Type": "application/json",
                    accept: "*/*"
                }
            })
            .then(response => {
                dispatch({
                    type: AUTH_LOGOUT_SUCCESS
                });
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: AUTH_LOGOUT_FAILURE,
                    payload: {
                        error: `${error.message} ... ${error.data.error}`
                    }
                });
            });
    };
};
