import {
    AUTH_LOGIN_LOADING,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAILURE,
    AUTH_LOGOUT_LOADING,
    AUTH_LOGOUT_SUCCESS,
    AUTH_LOGOUT_FAILURE
} from "../types";

// Helpers
import {getUserInfoLS} from '../../helpers/authHelper';


const userInfoInit = {
    tokenDetails: {
        access_token: "",
        expires_in: 0,
        token_type: ""
    },
    userDetails: null,
    authorities: [],
    isLoggedIn: false,
}

const initialState = {
    userInfo: getUserInfoLS(userInfoInit),
    isLoading: false,
    error: "",
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_LOGIN_LOADING: {
            console.log("logging in...");
            return {
                ...state,
                isLoading: true
            };
        }
        case AUTH_LOGIN_SUCCESS: {
            console.log("successfully logged in!!");
            let userInfo = {
                tokenDetails: action.payload.tokenDetails,
                isLoggedIn: true,
            }
            localStorage.setItem("userInfo", 
                JSON.stringify(userInfo)
            )
            return {
                userInfo,
                error: "",
                isLoading: false
            };
        }
        case AUTH_LOGIN_FAILURE: {
            console.log("login failed");
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            };
        }
        case AUTH_LOGOUT_LOADING: {
            console.log("logging out...");
            return {
                ...state,
                isLoading: true,
            };
        }
        case AUTH_LOGOUT_SUCCESS: {
            console.log("successfully logged out.");
            return {
                ...state,
                userInfo: userInfoInit,
                isLoading: false,
            };
        }
        case AUTH_LOGOUT_FAILURE: {
            console.log("logout failed.");
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            };
        }
        default:
            return state;
    }
};



export default authReducer;
