import { createStore, combineReducers } from "redux";

// Reducers
import authReducer from "./auth/reducer";

const rootReducer = combineReducers({
    authReducer
});

// Redux Store
const redux = require("redux");
const thunkMiddleware = require("redux-thunk").default;
const applyMiddleware = redux.applyMiddleware;

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default store;
