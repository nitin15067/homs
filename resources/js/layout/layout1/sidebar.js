import React from "react";
import { Link } from "react-router-dom";

// Font Awesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Sidebar = ({logout}) => {
    return (
        <ul
            className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
            id="accordionSidebar"
        >
            <a
                className="sidebar-brand d-flex align-items-center justify-content-center"
                href="index.html"
            >
                <div className="sidebar-brand-icon rotate-n-15">
                    <FontAwesomeIcon icon="hospital-symbol" size="2x" />
                </div>
                <div className="sidebar-brand-text mx-1">oMS</div>
            </a>

            <hr className="sidebar-divider my-0" />

            <li className="nav-item">
                <Link className="nav-link" to="/dashboard">
                    <FontAwesomeIcon className="mx-2" icon="tachometer-alt" />
                    <span>Dashboard</span>
                </Link>
            </li>

            <hr className="sidebar-divider" />

            <div className="sidebar-heading">Queues</div>

            <li className="nav-item">
                <Link className="nav-link" to="/waiting-queue">
                    <FontAwesomeIcon className="mx-2" icon="pause-circle" />
                    <span>Waiting</span>
                </Link>
            </li>

            <li className="nav-item">
                <Link className="nav-link" to="/skipped-queue">
                    <FontAwesomeIcon className="mx-2" icon="forward" />
                    <span>Skipped</span>
                </Link>
            </li>
            <hr className="sidebar-divider d-none d-md-block" />

            <div className="text-center d-none d-md-inline">
                <button
                    className="rounded-circle border-0"
                    id="sidebarToggle"
                    onClick={logout}
                >Logout</button>
            </div>
        </ul>
    );
};

export default Sidebar;
