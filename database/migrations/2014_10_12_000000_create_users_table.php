<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 20);
            $table->string('middlename', 20)->nullable();
            $table->string('lastname', 20)->nullable();
            $table->string('email', 30)->unique();
            $table->text('license')->nullable();
            $table->string('password', 100);
            $table->integer('department_id')->nullable();
            $table->integer('room_id')->nullable();
            $table->integer('counter_id')->nullable();
            $table->integer('user_type_id');
            $table->text('timings')->nullable();
            $table->integer('age');
            $table->integer('gender_id');
            $table->text('address')->nullable();
            $table->string('contact_no', 15); 
            $table->text('photo')->nullable(); 
            $table->nullableTimestamps();
            $table->integer('active_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
